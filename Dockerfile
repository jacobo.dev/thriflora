FROM node:alpine3.16
RUN mkdir -p /thriflora
WORKDIR /thriflora
COPY package.json package-lock.json /thriflora
RUN npm install
COPY . /thriflora
CMD npm run start