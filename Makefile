.PHONY: build start

build:
	docker build -t thriflora .

start: build
	docker run --rm -it -v $$(pwd)/src:/thriflora/src -p 4200:4200 thriflora

shell: build
	docker run --rm -it -v $$(pwd)/src:/thriflora/src thriflora bash

test: build
	docker run --rm -it -v $$(pwd)/src:/thriflora/src thriflora npm run test

test-watch: build
	docker run --rm -it -v $$(pwd)/src:/thriflora/src thriflora npm run test:watch

