# Thriflora

[Deploy web] https://thrifloratdd.web.app

[User History] https://gitlab.com/devscola/prueba-tecnica

[Repository of this project] https://gitlab.com/jacobo.dev/thriflora.git



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.2.


# Docker

## make build

Build the image


## make start

Deploy web in localhost:4200


## make test-watch

Run tests continually after every saved change inside /src


## make test

Run tests only once and finish


## Please rebuild docker image after changes outside /src folder



# Without docker

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `npm run test` to execute the unit tests via Jest & Testing Library

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

