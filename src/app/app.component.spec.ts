import '@testing-library/jest-dom'; // If we delete this line, sometimes it works fine but suddenly stops working with no reason
import { render, screen } from '@testing-library/angular'
import userEvent from '@testing-library/user-event';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { RegisterFormComponent } from './register-form/register-form.component';


describe('AppComponent', () => {
  let fixture: AppComponent
  let registerForm: RegisterFormComponent
  const emailPlaceholder = 'usuario@dominio.com'
  const validEmail = 'hola@gmail.com'
  const invalidEmail = 'hola@gmaaaaaaail'

  const renderComponents = async () => {
    return await render( AppComponent, { 
      declarations: [ RegisterFormComponent ],
      imports: [ ReactiveFormsModule ]
    })
  }

  const userRender = async () => {
    const user = userEvent.setup()
    await renderComponents()
    return user
  }


  it("has the name of the store as heading", async () => {
    fixture = new AppComponent()
    await renderComponents()

    const header = screen.getByText( fixture.title )

    expect( header ).toBeInTheDocument()
  })


  it("email input appears", async () => {
    await renderComponents()

    const emailInput = screen.getByPlaceholderText( emailPlaceholder )

    expect( emailInput ).toBeInTheDocument()
  })


  it("alert message displayed when user exits email input without typing", async () => {
    const user = await userRender()

    await user.click( screen.getByPlaceholderText( emailPlaceholder ) )
    await user.click( screen.getByText( fixture.title ) )

    expect( screen.getByText( "El email es necesario para poder continuar." ) ).toBeInTheDocument()
  })


  it("alert message displayer when user leaves email input with invalid value", async () => {
    const user = await userRender()

    await user.type( screen.getByPlaceholderText( emailPlaceholder ), invalidEmail )
    await user.click( screen.getByText( fixture.title ) )
    
    expect( screen.getByText( "Introduzca un email válido por favor." ) ).toBeInTheDocument()
  })

  
  it("no alert message displayer while typing", async () => {
    const user = await userRender()

    await user.type( screen.getByPlaceholderText( emailPlaceholder ), invalidEmail )
    
    
    expect( screen.queryByText( "El email es necesario para poder continuar." ) ).not.toBeInTheDocument()
    expect( screen.queryByText( "Introduzca un email válido por favor." ) ).not.toBeInTheDocument()
  })


  it("displayed alert messages dissapear after fixing errors", async () => {
    const user = await userRender()

    await user.click( screen.getByPlaceholderText( emailPlaceholder ) )
    await user.click( screen.getByText( fixture.title ) )
    expect( screen.getByText( "El email es necesario para poder continuar." ) ).toBeInTheDocument()

    await user.type( screen.getByPlaceholderText( emailPlaceholder ), invalidEmail )
    expect( screen.queryByText( "El email es necesario para poder continuar." ) ).not.toBeInTheDocument()
    expect( screen.getByText( "Introduzca un email válido por favor." ) ).toBeInTheDocument()

    await user.clear( screen.getByPlaceholderText( emailPlaceholder ) )
    await user.type( screen.getByPlaceholderText( emailPlaceholder ), validEmail )
    await user.click( screen.getByText( fixture.title ) )
    expect( screen.queryByText( "El email es necesario para poder continuar." ) ).not.toBeInTheDocument()
    expect( screen.queryByText( "Introduzca un email válido por favor." ) ).not.toBeInTheDocument()
  })


  it("country select appears", async () => {
    registerForm = new RegisterFormComponent()
    await renderComponents()

    const selectCountry = screen.getByText( registerForm.defaultSelect )

    expect(selectCountry).toBeTruthy()
  })

  xit("displayed alert when user opens select options and leaves without selecting one", async () => {
    registerForm = new RegisterFormComponent()
    const user = await userRender()

    await user.click( screen.getByText( registerForm.defaultSelect ) )
    await user.click( screen.getByText( fixture.title ) )

    expect( screen.queryByText( "Por favor, seleccione un país." ) ).toBeInTheDocument()
  })

  it("checkbox appears", async () => {
    await renderComponents()
    expect( screen.queryByText( "Quiero recibir un email cuando la tienda esté disponible" ) ).toBeTruthy()
  })

  it("checkbox displays error when user enables and disables", async () => {
    const user = await userRender()

    await user.click( screen.getByRole( 'checkbox', { hidden: true } ) )
    await user.click( screen.getByRole( 'checkbox', { hidden: true } ) )

    expect( screen.queryByText( "Es necesario que confirmes." ) ).toBeTruthy()
  })

  it("displayed checkbox error disappears after fixing errors", async () => {
    const user = await userRender()

    await user.click( screen.getByRole( 'checkbox', { hidden: true } ) )
    await user.click( screen.getByRole( 'checkbox', { hidden: true } ) )
    await user.click( screen.getByRole( 'checkbox', { hidden: true } ) )

    expect( screen.queryByText( "Es necesario que confirmes." ) ).toBeFalsy()
  })

  it("submit button for the form appears", async () => {
    await renderComponents()
    expect( screen.queryByText( "¡Quiero ser la primera en comprar!" ) ).toBeTruthy()
  })

  it("submit button is disabled at start", async () => {
    await renderComponents()
    const button = screen.queryByText( "¡Quiero ser la primera en comprar!" ) as HTMLButtonElement
    expect( button.disabled ).toBeTruthy()
  })

  xit("submit button ONLY enables when every required value in form is valid", async () => {
    const user = await userRender()
    const button = screen.queryByText( "¡Quiero ser la primera en comprar!" ) as HTMLButtonElement

    await user.type( screen.getByPlaceholderText( emailPlaceholder ), validEmail )
    expect( button.disabled ).toBeTruthy()

    // Cannot try user event for select options
    // Temporary checked configuring country as not required and works fine
    expect( button.disabled ).toBeTruthy()

    await user.click( screen.getByRole( 'checkbox', { hidden: true } ) )
    expect( button.disabled ).toBeFalsy()
  })
});
