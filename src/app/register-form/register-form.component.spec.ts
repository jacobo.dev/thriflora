import '@testing-library/jest-dom'; // If we delete this line, sometimes it works fine but suddenly stops working with no reason
import userEvent from '@testing-library/user-event'
import { render, screen, waitFor } from '@testing-library/angular'
import { ReactiveFormsModule } from '@angular/forms';

import { RegisterFormComponent } from './register-form.component';


describe('RegisterFormComponent', () => {
  const emailPlaceholder = 'usuario@dominio.com'
  const validEmail = 'hola@gmail.com'
  const invalidEmail = 'hola@gmaaaaaaail'
  const testText: string = "texto"
  const fixture = new RegisterFormComponent()

  const renderComponents = async () => {
    return await render ( RegisterFormComponent, {
      imports: [ ReactiveFormsModule ]
    })
  }

  const userRender = async () => {
    const user = userEvent.setup()
    await renderComponents()
    return user
  }

  it("receives email input value", async () => {
    const user = await userRender()

    await user.type( screen.getByPlaceholderText( emailPlaceholder ), testText )
    const receivedInput = fixture.getEmailValue()

    expect( receivedInput ).toBe( testText )
  })
  

  xit("displays confirmation message after successful submit", async () => {
    await renderComponents()

    fixture.submit()

    await waitFor( () => {
      expect( screen.getByText( "¡Perfecto, te avisaremos la primera!", { exact: false }) ).toBeInTheDocument()
    })
  })

  it("gets form values when valid submit", async () => {
    const user = await userRender()

    await user.type( screen.getByPlaceholderText( emailPlaceholder ), validEmail )
    await user.click( screen.getByRole( 'checkbox', { hidden: true } ) )
    // cannot check select option with tests so far
    // Works fine with console.log
  })
});
