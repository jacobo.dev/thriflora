import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent {  
  emailPlaceholder: string = 'usuario@dominio.com'
  defaultSelect: string = 'Seleccionar país...'
  userForm: FormGroup
  valid: boolean = false

  constructor() {
    this.userForm = new FormGroup({
      userEmail: new FormControl( '', [ Validators.required, Validators.pattern( "^[a-zA-Z0-9._-]+@+[a-zA-Z0-9.-]+[\.]+[a-zA-Z]{2,4}$" ) ] ),
      country: new FormControl( '', Validators.required),
      userCheck: new FormControl( '', Validators.required)
    })
  }

  getEmailValue() {
    return ( <HTMLInputElement>document.getElementById("idEmail") ).value
  }

  submit() {
    // window.alert("¡Perfecto, te avisaremos la primera!")
    this.valid = true
  }

  save(value: any) {
    console.log(value)
    this.submit()
  }
}
